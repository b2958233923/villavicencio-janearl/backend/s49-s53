const Product = require("../models/Product");




module.exports.addProduct = (req, res) => {

	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		imgUrl: req.body.imgUrl
	})

	return newProduct.save().then((product, error) => {

		if(product){
			return res.send(true);
		} else {
			return res.send(false);
		}
	}).catch(err => res.send(err))
};	


module.exports.getAllProducts = (req, res) => {
	return Product.find({}).then(result => {
		
		return res.send(result);
	})
	.catch(err => res.send(err))
};


module.exports.getAllActive = (req, res) => {
	return Product.find({ isActive: true }).then(result =>{
		
		return res.send(result)
	})
	.catch(err => res.send(err))
};


module.exports.getProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result => {
		
		return res.send(result)
	})
	.catch(err => res.send(err))
};



module.exports.updateProduct = (req, res) =>{

	
	let updatedProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		imgUrl: req.body.imgUrl
	}

	
	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {

		
		if(error) {
			return res.send(false);
		
		} else {
			
			return res.send(true);
		}
	})
	.catch(err => res.send(err))
};


module.exports.archiveProduct = (req, res) => {

	let archivedProduct = {
		isActive: false
	}
	return Product.findByIdAndUpdate(req.params.productId, archivedProduct).then((product, error) => {
		
		if(error){
			
			return res.send(false);
		} else {
			
			return res.send(true);
		}
	})
	.catch(err => res.send(err))
};


module.exports.activateProduct = (req, res) => {

	let activatedProduct = {
			isActive: true
		}
	
	return Product.findByIdAndUpdate(req.params.productId, activatedProduct).then((product, error) => {
		
		if(error){
			
			return res.send(false);
		} else {
			
			return res.send(true);
		}
	})
	.catch(err => res.send(err))
};


// Controller action to search for courses by course name
module.exports.searchProductsByName = async (req, res) => {
	try {
	  const { productName } = req.body;
  
	  // Use a regular expression to perform a case-insensitive search
	  const products = await Product.find({
		name: { $regex: productName, $options: 'i' }
	  });
  
	  res.json(products);
	} catch (error) {
	  console.error(error);
	  res.status(500).json({ error: 'Internal Server Error' });
	}
  };

  // Delete product
module.exports.deleteProduct = async (req, res) => {
	try {
	  const productId = req.params.id;
  
	  // Check if the product exists
	  const product = await Product.findById(productId);
	  if (!product) {
		return res.status(404).json({ error: "Product not found" });
	  }
  
	  // Perform the deletion
	  await Product.findByIdAndDelete(productId);
  
	  // Return success response
	  res.json({ success: true });
	} catch (error) {
	  res.status(500).json({ error: "Error deleting product" });
	}
  };



  module.exports.searchProductsByPriceRange = async (req, res) => {
	try {
	  const { minPrice, maxPrice } = req.body;
 
		 // Find products within the price range
	  const products = await Product.find({
	   price: { $gte: minPrice, $lte: maxPrice }
	 });
	 
	 res.status(200).json({ products });
   } catch (error) {
	 res.status(500).json({ error: 'An error occurred while searching for products' });
   }
  };