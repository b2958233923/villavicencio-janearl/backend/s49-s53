// Dependencies and Modules
const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		// The "find" metho
		// returns a record if a match is found.
		if(result.length > 0) {
			return true; //"Duplicate email found"
		} else {
			return false;
		}
	})
};


// Register a User function
module.exports.registerUser = (req, res) => {


	let newUser = new User({
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 8)
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return res.send(false);

		} else {
			return res.send(true);
		}

	}).catch(err => res.send(err))
};

// Login User Function by email
module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {
		
		if(result == null){
			return false
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect){
				
				return res.send({access: auth.createAccessToken(result)});
			} else {
				
				return res.send(false);
			}
		}
	}).catch(err => res.send(err));
};




// User User Details Retrieve Function

module.exports.getDetails = (req, res) => {

	return User.findById(req.user.id).then(result => {
			
			result.password = "";
			return res.send(result);
				
	}).catch(err => res.send(err));
};

module.exports.updateProfile = async (req, res) => {
	try {
	  // Get the user ID from the authenticated token
	  const userId = req.user.id;
  
	  // Retrieve the updated profile information from the request body
	  const { email } = req.body;
  
	  // Update the user's profile in the database
	  const updatedUser = await User.findByIdAndUpdate(
		userId,
		{ email },
		{ new: true }
	  );
  
	  res.json(updatedUser);
	} catch (error) {
	  console.error(error);
	  res.status(500).json({ message: 'Failed to update profile' });
	}
  };
  


// Function to Set User as Admin
module.exports.setUserAsAdmin = async (req, res) => {
  const { userId } = req.body;

  try {
    const user = await User.findByIdAndUpdate(userId, { isAdmin: true });

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    return res.status(200).json({ message: 'User updated as admin' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};




// Function to reset the password
module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

  