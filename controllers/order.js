// Dependencies and Modules

const Product = require("../models/Product");
const Order = require("../models/Order");
const User = require("../models/User");



// // Function to create an order
module.exports.createOrder = async (req, res) => {
  try {
    const { userId, products } = req.body;

    // Validate if the user exists and is not an admin
    const user = await User.findById(userId);
    if (!user || user.isAdmin) {
      return res.json(false);
    }

    const orderedProducts = [];
    let totalAmount = 0;

    for (const product of products) {
      const foundProduct = await Product.findById(product.productId);
      if (!foundProduct) {
        return res.json(false);
      }

      const orderedProduct = {
        productId: product.productId,
        name: product.name,
        quantity: product.quantity,
        
      };

      orderedProducts.push(orderedProduct);
      totalAmount += foundProduct.price * product.quantity;
    }

    // Create the order
    const order = new Order({
      userId,
      products: orderedProducts,
      totalAmount,
    });

    const savedOrder = await order.save();
    res.json(true);
  } catch (error) {
    res.json(false);
  }
};




// Function to retrieve orders of user
module.exports.getUserOrders = async (req, res) => {
           const userId = req.user.id; // Assuming the user ID is available in the req.user.id property

  try {
    // Find the orders for the user
    const orders = await Order.find({ userId });

    if (orders.length === 0) {
      return res.status(404).json({ message: 'No orders found for the user' });
    }

    res.status(200).json({ orders });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving user orders' });
  }
};

module.exports.getAllOrders = async (req, res) => {
  try {
    // Find all orders
    const orders = await Order.find();

    if (orders.length === 0) {
      return res.status(404).json({ message: 'No orders found' });
    }

    res.status(200).json({ orders });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving orders' });
  }
};