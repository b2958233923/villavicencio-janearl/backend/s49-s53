// // Dependencies and Modules
// const Product = require('../models/Product');
// const Cart = require('../models/Cart');

// // module.exports.addToCart = async (req, res) => {
// //   try {
// //     const productId = req.body.productId;
// //     const name = req.body.name;
// //     const quantity = req.body.quantity;

// //     // Find the product in the database
// //     const product = await Product.findOne({ _id: productId });

// //     if (!product) {
// //       return res.send('Product not found');
// //     }

// //     // Check if the user already has a cart
// //     const existingCart = await Cart.findOne({ userId: req.user.id });

// //     if (existingCart) {
// //       // Update the existing cart
// //       const existingItem = existingCart.products.find(item => item.productId === productId);

// //       if (existingItem) {
// //         existingItem.quantity += quantity;
// //       } else {
// //         existingCart.products.push({ productId, name, quantity });
// //       }

// //       await existingCart.save();
// //     } else {
// //       // Create a new cart
// //       const newCart = new Cart({
// //         userId: req.user.id,
// //         products: [{ productId, name, quantity }],
        
// //       });

// //       await newCart.save();
// //     }

// //     // Get the cart amount
// //     const cartAmount = await Cart.aggregate([
// //       { $match: { userId: req.user.id } },
// //       { $unwind: '$products' },
// //       { $group: { _id: null, total: { $sum: '$products.quantity' } } }
// //     ]);

// //     res.send({ success: true, cartAmount: cartAmount[0].total });
// //   } catch (error) {
// //     res.send({ success: false, error: error.message });
// //   }
// // };


// const addToCart = async (req, res) => {
  
//   try {
// // Get the logged-in user's ID from the request object (Assuming it's stored in req.user.id after successful login)
// const userId = req.user.id;

// const { productId, quantity } = req.body;

// // Check if the product exists
// const product = await Product.findById(productId);
// if (!product) {
//   return res.status(404).json({ error: 'Product not found' });
// }

// // Fetch the product name from the fetched product
// const prodName = product.name; // Assuming the product name field is called 'name' in the Product model

// // Find the user's cart or create a new one if it doesn't exist
// let cart = await Cart.findOne({ userId });
// if (!cart) {
//   cart = new Cart({
//     userId,
//     products: [{ productId, prodName, quantity }],
//     totalAmount: product.price * quantity, // Assuming each product has a "price" field in the Product model
//   });
// } else {
//   // Check if the product is already in the cart
//   const existingProduct = cart.products.find((p) => p.productId === productId);
//   if (existingProduct) {
//     existingProduct.quantity += quantity;
//   } else {
//     cart.products.push({ productId, prodName, quantity });
//   }
//   cart.totalAmount += product.price * quantity;
// }

// await cart.save();

// return res.status(200).json({ message: 'Product added to cart successfully' });
// } catch (err) {
// console.error('Error adding to cart:', err);
// res.status(500).json({ error: 'Internal server error' });
// }
// }

// module.exports = { addToCart };





// module.exports.getUserCarts = async (req, res) => {
//   try {
//     // Find the cart items for the authenticated user
//     const carts = await Cart.find({ userId: req.user.id });

//     if (carts.length === 0) {
//       return res.status(404).json({ message: 'No item in the cart found for the user' });
//     }

//     res.status(200).json({ carts });
//   } catch (error) {
//     res.status(500).json({ message: 'An error occurred while retrieving cart items' });
//   }
// };





// // Function to change the quantity of a product in the cart
// module.exports.changeProductQuantity = async (req, res) => {
//   try {
//     const productId = req.params.productId;
//     const quantity = req.body.quantity;

//     // Find the user's cart
//     const cart = await Cart.findOne({ userId: req.user.id });

//     if (!cart) {
//       return res.json({ success: false, error: 'Cart not found' });
//     }

//     // Find the product in the cart
//     const productItem = cart.products.find(item => item.productId === productId);

//     if (!productItem) {
//       return res.json({ success: false, error: 'Product not found in cart' });
//     }

//     // Update the quantity of the product
//     productItem.quantity = quantity;
//     await cart.save();

//     res.json({ success: true });
//   } catch (error) {
//     res.json({ success: false, error: error.message });
//   }
// };




// // Removing Products from Cart
// module.exports.removeProductFromCart = async (req, res) => {
//   try {
//     const productId = req.params.productId;

//     // Find the user's cart
//     const cart = await Cart.findOne({ userId: req.user.id });

//     if (!cart) {
//       return res.json({ success: false, error: 'Cart not found' });
//     }

//     // Find the product in the cart
//     const productItemIndex = cart.products.findIndex(item => item.productId === productId);

//     if (productItemIndex === -1) {
//       return res.json({ success: false, error: 'Product not found in cart' });
//     }

//     // Remove the product from the cart
//     cart.products.splice(productItemIndex, 1);
//     await cart.save();

//     res.json({ success: true });
//   } catch (error) {
//     res.json({ success: false, error: error.message });
//   }
// };




// // Getting the subtotal for Product
// module.exports.getProductSubtotal = async (req, res) => {
//   try {
//     const userId = req.user.id;

//     // Find the cart for the user
//     let cart = await AddToCart.findOne({ userId });

//     if (!cart) {
//       return res.status(404).json({ error: 'Cart not found' });
//     }

//     const getProductDetails = async (productId) => {
//       try {
//         const product = await Product.findById(productId);

//         if (!product) {
//           throw new Error('Product not found');
//         }

//         return {
//           productName: product.name,
//           productPrice: product.price,
//         };
//       } catch (error) {
//         console.error(error);
//         throw new Error('Error fetching product details');
//       }
//     };

//     // Calculate subtotal for each item
//     const itemsSubtotal = await Promise.all(
//       cart.products.map(async (product) => {
//         const { productId, quantity } = product;
//         const { productName, productPrice } = await getProductDetails(productId);
//         const itemSubtotal = productPrice * quantity;
//         return { productId, productName, quantity, itemSubtotal };
//       })
//     );

//  // Calculate total price
//  const totalPrice = itemsSubtotal.reduce((acc, item) => acc + item.itemSubtotal, 0);

//  res.json({ itemsSubtotal, totalPrice });
// } catch (error) {
//  res.status(500).json({ error: 'Internal server error' });
// }
// };


const Product = require("../models/Product");
const Order = require("../models/Order");
const User = require("../models/User")
const Cart = require("../models/Cart")
const auth=require('../auth');

const addProductToCart = async (req, res) => {
  
      try {
    // Get the logged-in user's ID from the request object (Assuming it's stored in req.user.id after successful login)
    const userId = req.user.id;

    const { productId, quantity } = req.body;

    // Check if the product exists
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ error: 'Product not found' });
    }

    // Fetch the product name from the fetched product
    const prodName = product.name; // Assuming the product name field is called 'name' in the Product model

    // Find the user's cart or create a new one if it doesn't exist
    let cart = await Cart.findOne({ userId });
    if (!cart) {
      cart = new Cart({
        userId,
        products: [{ productId, prodName, quantity }],
        totalAmount: product.price * quantity, // Assuming each product has a "price" field in the Product model
      });
    } else {
      // Check if the product is already in the cart
      const existingProduct = cart.products.find((p) => p.productId === productId);
      if (existingProduct) {
        existingProduct.quantity += quantity;
      } else {
        cart.products.push({ productId, prodName, quantity });
      }
      cart.totalAmount += product.price * quantity;
    }

    await cart.save();

    return res.status(200).json({ message: 'Product added to cart successfully' });
  } catch (err) {
    console.error('Error adding to cart:', err);
    res.status(500).json({ error: 'Internal server error' });
  }
}

module.exports = { addProductToCart };




module.exports.updateProductQuantity = async (req, res) => {
 try {
     // Get the logged-in user's ID from the request object (Assuming it's stored in req.user.id after successful login)
    const userId = req.user.id;

    const { productId } = req.params;
    const { quantity } = req.body;

    // Check if the product exists
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ error: 'Product not found' });
    }

    // Find the user's cart
    const cart = await Cart.findOne({ userId });
    if (!cart) {
      return res.status(404).json({ error: 'Cart not found' });
    }

    // Check if the product is in the cart
    const cartProduct = cart.products.find((p) => p.productId === productId);
    if (!cartProduct) {
      return res.status(404).json({ error: 'Product not found in the cart' });
    }

    // Calculate the difference in quantity for the product
    const quantityDifference = quantity - cartProduct.quantity;

    // Update the quantity of the product
    cartProduct.quantity = quantity;

    // Update the total amount in the cart
    cart.totalAmount += quantityDifference * product.price;

    // Save the updated cart
    await cart.save();

    return res.status(200).json({ message: 'Cart quantity updated successfully' });
  } catch (err) {
    console.error('Error updating cart quantity:', err);
    res.status(500).json({ error: 'Internal server error' });
  }
}

module.exports.removeProductFromCart = async (req, res) => {
  try {
    const userId = req.user.id;
    const { productId } = req.body;

    // Find the cart for the user
    let cart = await Cart.findOne({ userId });

    if (!cart) {
      return res.json(false);
    }

    // Find the index of the product in the cart
    const productIndex = cart.products.findIndex((product) => product.productId === productId);

    if (productIndex === -1) {
      return res.json(false);
    }

    // Retrieve the quantity and price of the product being removed
    const removedQuantity = cart.products[productIndex].quantity;
    const productPrice = cart.products[productIndex].price || 0; // Handle the case when the price is missing or falsy

    // Update the total amount by subtracting the product's cost (price * quantity) from the current total
    cart.totalAmount -= productPrice * removedQuantity;

    // Remove the product from the cart
    cart.products.splice(productIndex, 1);

    // If there are no products left in the cart, delete the cart from the database
    if (cart.products.length === 0) {
      await Cart.deleteOne({ userId });
    } else {
      // Otherwise, save the updated cart
      await cart.save();
    }

    res.json(true);
  } catch (error) {
    res.json(false);
  }
};

module.exports.getCartSubtotal = async (req, res) => {
  try {
    const userId = req.user.id;

    // Find the cart for the user
    let cart = await Cart.findOne({ userId });

    if (!cart) {
      return res.status(404).json({ error: 'Cart not found' });
    }

    const getProductDetails = async (productId) => {
      try {
        const product = await Product.findById(productId);

        if (!product) {
          throw new Error('Product not found');
        }

        return {
          productName: product.name,
          productPrice: product.price,
        };
      } catch (error) {
        console.error(error);
        throw new Error('Error fetching product details');
      }
    };

    // Calculate subtotal for each item
    const itemsSubtotal = await Promise.all(
      cart.products.map(async (product) => {
        const { productId, quantity } = product;
        const { productName, productPrice } = await getProductDetails(productId);
        const itemSubtotal = productPrice * quantity;
        return { productId, productName, quantity, itemSubtotal };
      })
    );

    // Calculate total price
    const totalPrice = itemsSubtotal.reduce((acc, item) => acc + item.itemSubtotal, 0);

    res.json({ itemsSubtotal, totalPrice });
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};