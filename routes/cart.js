// // Dependencies and Modules
// const express = require('express');
// const cartController = require('../controllers/cart');
// const auth = require('../auth');

// const { verify, verifyAdmin } = auth;

// const router = express.Router();


// // Route for user to add to cart
// router.post("/", verify, cartController.addToCart)

// // [SECTION] Route to get users all items in cart
// router.get("/all-user-cart", verify, cartController.getUserCarts);

// // Route for user to change product quantity in cart
// router.put('/:productId/change-quantity', verify, cartController.changeProductQuantity);

// // Route for user to remove product from cart
// router.delete('/:productId/remove', verify, cartController.removeProductFromCart);

// // Route for getting subtotal for each product in cart
// router.get('/subtotal', verify, cartController.getProductSubtotal);










// module.exports = router;


const express = require('express');
const addToCartController = require('../controllers/cart')
const router = express.Router();
const auth = require('../auth');

const {verify, verifyAdmin} = auth;

router.post("/",verify,addToCartController.addProductToCart)


router.put("/changeQuantity/:productId", verify, addToCartController.updateProductQuantity)


router.delete('/removeProduct/', verify, addToCartController.removeProductFromCart);

router.get('/getCartSubtotal', verify, addToCartController.getCartSubtotal)

module.exports = router;