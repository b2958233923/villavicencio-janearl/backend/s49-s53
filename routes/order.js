// Dependencies and Modules
const express = require('express');
const orderController = require('../controllers/order');
const auth = require('../auth');

const { verify, verifyAdmin } = auth;

const router = express.Router();


// Route for user to place an order
router.post("/create-order", verify, orderController.createOrder);


// [SECTION] Route to get users orders
router.get("/user-orders", verify, orderController.getUserOrders);

// [SECTION] Route to get all orders (for admin)
router.get("/all-orders", verify, verifyAdmin, orderController.getAllOrders);






module.exports = router;