// Dependencies and Modules
const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');

const { verify, verifyAdmin } = auth;

const router = express.Router();


// Route for checking if user email already exist.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", userController.registerUser);

// Route for login
router.post("/login", userController.loginUser);

// Route for user details
router.get("/details", verify, userController.getDetails);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);


// Update user as admin
router.put('/set-as-admin', verify, verifyAdmin, userController.setUserAsAdmin);


// Route for resetting the password
router.put('/reset-password', verify, userController.resetPassword);





module.exports = router;