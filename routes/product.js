const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');
const { verify, verifyAdmin } = auth;

const router = express.Router();


// Adding a Product
router.post("/", verify, verifyAdmin, productController.addProduct);

// Retrieve All products 
router.get("/all", productController.getAllProducts);

// Retrieve Active products
router.get("/", productController.getAllActive);

// Route to search for products by product name
router.post('/search', productController.searchProductsByName);

//[ACTIVITY] Search Products By Price Range
router.post('/search-by-price', productController.searchProductsByPriceRange);

// Retrieve specific product
router.get("/:productId/specific", productController.getProduct);

// Updating Product Information (Admin)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// Route for product archiving
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// Route for product activation
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

// Delete product
router.delete("/:id", verify, verifyAdmin, productController.deleteProduct);

module.exports = router;