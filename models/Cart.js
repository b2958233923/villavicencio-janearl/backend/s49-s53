// // Mongoose Dependency
// const mongoose = require('mongoose');

// // Schema/Blueprint
// const cartSchema = new mongoose.Schema({

// 	userId: {
// 		type: String,
// 		required: [true, 'User ID is required']
// 	},
// 	products: [{
// 		productId: {
// 			type: String,
// 			required: [true, 'Product ID is required']
// 		},
// 		prodName: {
// 			type: String,
			
// 		},
// 		quantity: {
// 			type: Number,
// 			required: [true, 'Order Quantity is required']
// 		}
// 	}],
// 	totalAmount: {
// 		type: Number,
// 		required: [true,'Total Amount is required']
// 	},
// 	addedOn: {
// 		type: Date,
// 		default: new Date()
// 	}
// })


// // Model
// module.exports = mongoose.model('Cart', cartSchema);


const mongoose = require('mongoose')

// Schema
const addToCartSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true,'User ID is required']
  },
  products: [
    {
      productId: {
        type: String,
        required: [true,'Product Id is required']
      },
      prodName: {
        type: String,
        
      },
      quantity: {
        type: Number,
        required: [true,'Quantity is required']
      }
    }
  ],
  totalAmount: {
    type: Number,
    required: [true,'Total Amount is required']
  },
  addedOn: {
    type: Date,
    default: Date.now,
  }
});

module.exports= mongoose.model('AddToCart', addToCartSchema);