// Mongoose Dependency
const mongoose = require('mongoose');

// Schema/Blueprint
const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, 'User ID is required']
	},
	products: [{
		productId: {
			type: String,
			required: [true, 'Product ID is required']
		},
		name: {
			type: String,
			required: [true, 'Order Name is required']
		},
		quantity: {
			type: Number,
			required: [true, 'Order Quantity is required']
		}
	}],
	totalAmount: {
		type: Number,
		required: [true, 'Total Amount is required']
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})


// Model
module.exports = mongoose.model('Order', orderSchema);